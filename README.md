# <div align="center">Shaumik Ganguly</div>

<img align="right" src="https://gitlab.com/uploads/-/system/user/avatar/1142548/avatar.png">

*M.Sc. Communications Engineering* <br />
*RWTH Aachen, Germany* <br />
*2014-2017*

*B.Tech. Electronics & Communications Engineering* <br />
*Amity University Noida, India* <br />
*2010-2014*

Professional interests: *Computer Vision, Machine Learning <br />*
Contact: *shaumik1@gmail.com*

---

### Brief

An enthusiast in the field of Computer Vision and Machine Learning, with a couple of years of practical experience in the relevant field,
with projects ranging in various image modalities and video processing applications, algorithms ranging from traditional approaches to
the state of the art neural network based implementations, with experience in various programming languages like Python, C#, C/C++, MATLAB
and several ML frameworks.

Currently, I am looking for a suitable job opportunity to build a career in the field of Computer Vision and Machine Learning.

A formal profile description of myself can be found [here](https://www.linkedin.com/in/shaumik-ganguly-699a9535/). <br />
A more illustrative description of my practical experiences have been hyperlinked to the respective list items below.

---

### Projects & Internships

* [Master Thesis: **Lesion segmentation in breast MRI using Convolutional Neural Networks**,
Institute of Imaging & Computer Vision, RWTH Aachen (Nov'16-May'17)](segmentation_CNN.md)
* [Student Worker: **Deep Learning for Medical Data**, Philips GmbH Aachen (May'16-Sept'16)](medical_imaging.md#deep-learning-of-medical-images)
* [Intern: **Medical Imaging & XNAT Interface Development**, Philips GmbH Aachen (Nov'16-May'16)](medical_imaging.md#medical-imaging-and-xnat-interface-development)
* [Student Worker: **Medical Imaging in Imalytics**, Philips GmbH Aachen (May'15-Oct'15)](medical_imaging.md#medical-imaging-in-imalytics)
* [Bachelor Thesis: **Automated detection and grading of Diabetic Retinopathy**, Amity University Noida (Aug'13-May'14)](diabetic_retinopathy.md)
* [Intern : **Video Deinterlacing project**, Institut Superieur d’Electronique de Paris, (May'13-June'13)](video_deinterlace.md)

---

### Publications

* [**An Efficient Image Processing Based Technique for Comprehensive
Detection and Grading of Non-Proliferative Diabetic Retinopathy
from Fundus Images**, *Computer Methods in Biomechanics and Biomedical Engineering: Imaging & Visualization*,
Received 23 Jun 2014, Accepted 11 May 2015, Published online: 01 Jul 2015, 
Volume 5, 2017 - Issue 3](http://www.tandfonline.com/doi/abs/10.1080/21681163.2015.1051187)

* [**An Overlapped Motion Compensated Approach for Video
Deinterlacing**, *International Conference on Computational Collective Intelligence*,
ICCCI 2014](https://link.springer.com/chapter/10.1007/978-3-319-11289-3_65)

* ...