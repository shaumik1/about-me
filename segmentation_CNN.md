### Segmentation of lesions using CNN

##### Preface

Carried out as part of master thesis.
The project was carried out in the Institute of Imaging and Computer Vision, RWTH Aachen from Nov 16 to May 2017.

##### Contribution

A full semantic segmentation of lesions in breast MRI was performed. Various critical aspects of a neural network 
were evaluated, presenting the impact of various pooling techniques, convolution kernels and placement of activation functions.
Importance of loss function, augmentation, regularization, etc were also higlighted.


##### Implementation

Scripting was done using Python. Various machine learning frameworks were adopted like Keras and Tensorflow. Extensive use of Tensorboard
visualization was made to project the various loss and accuracy metrics, visualize the convolution feature maps, model architecture, histograms, etc.

*A more detailed insight to the project shall be made public in the recent future !*