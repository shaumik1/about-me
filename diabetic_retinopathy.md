### Diabetic Retinopathy (automated detection and grading)

##### Preface

Carried out as part of the final year project during the bachelor's degree.
The project was carried out in Amity School of Engineering & Technology,Noida with close collaboation with Venu Eye Research Centre, New Delhi, from Aug 2013 to May 2014.

##### Contribution

[The work was published at several reputed platforms, with the most recent and comprehensive version by the Taylor and Francis journal
in the Computer Methods in Biomechanics and Biomedical Engineering: Imaging & Visualization 
Volume 5, 2017 - Issue 3, Published online: 01 Jul 2015 ](http://www.tandfonline.com/doi/abs/10.1080/21681163.2015.1051187) <br />
This work was part of a bigger project on Design and Development of Computer Aided Diagnosis of Eye Diseases (Diabetic Retinopathy & Glaucoma)
and Watermarking of Medical Images for Tele-Ophthalmology, which received funding from the Department of Science & Technology (DST), New Delhi. <br />
The main contributions of this work are: implementation of a region-based computation to reduce the overall computational complexity, innovative
approaches for the detection of various symptoms of Diabetic Retinopathy (DR) such as exudates, microaneurysms (or red lesions).
An important contribution of this work was the flexibility of the framework to implement other algorithms for detecting symptoms and still ensuring
the aspect of reduced computations.

##### Motivation

DR is one of the unique eye diseases which affects patients suffering from diabetes. In the preliminary stages, symptoms of DR are not visible to the naked eye
and the patient does not experience vision loss. However, later stages may already render an irreparable damage to the eye, thus necessitating an early
detection mechanism. Use of fundus images of an eye retina, as shown in the figure with various symptoms of DR, is done by Ophthalmologists. This work attempts to
provide an automated solution to make the process faster and cost effective, so that early screening of DR may become practical in the coming future.

<img align="centre" src="https://i2.wp.com/www.michaelduplessie.com/wp-content/uploads/2013/12/diabetic-retinopathy-fundus-photo.jpg?ssl=1" width="300" height="250">



##### Implementation

The algorithm was implemented on MATLAB with the implementation of a GUI, as shown in the figure.

<img align="centre" src="./images/bachelor_GUI_image.jpg" width="500" height="300">

 A detailed description of the algorithm can be found in the article mentioned above.