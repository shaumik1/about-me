### Video Deinterlacing

##### Preface

Carried out as part of a mandatory internship during the bachelor's degree.
The project was carried out in the research laboratory of the Institut Supérieur d’Electronique de Paris, located in the heart of Paris in May-June 2013.

##### Contribution

[The work has been published in the *International Conference on Computational Collective Intelligence*,
ICCCI 2014](https://link.springer.com/chapter/10.1007/978-3-319-11289-3_65) <br />
Two main contributions of this work are: use Optical Flow predictor and block based motion estimation for high texture 
and low texture blocks respectively, implementation of an Overlapped block motion compensated approach.

##### Motivation

Interlaced video have been preferred for long due to its satisfactory trade off between frame rate and bandwidth capacity.
However, modern day digital display devices have fixed resolution units, thereby making it imperative to perform a suitable
interpolation, referred as deinterlacing to obtain full sized frames from half resolution fields, as shown in the figure.

<img align="centre" src="http://www.ifp.illinois.edu/~yuhuang/realtek/deinterlacing.jpg" width="400" height="175">
<img align="centre" src="http://download.videohelp.com/dvdforger/help/howto_3_step_4.gif" width="400" height="175">



##### Implementation

The algorithm was implemented on MATLAB. The algorithm first performed a shot-cut detection to make a decision if the interpolation of the current frame
should be performed using only forward motion vectors (MV), backward MV, both or none. Following which every frame was subjected to a canny edge detector
and partitioned into blocks, to ascertain each frame consisting of high texture blocks (with large number of edges) and low texture/smooth blocks. Successively,
use of a Block-based Motion Estimation is performed in smooth blocks of the frame and Optical Flow predictor is used on the high texture blocks. Eventually,
an Overlapped Block Motion Compensation is performed.

<img align="centre" src="./images/bachelor_intern.jpg" width="400" height="300">