### Deep learning of Medical Images

##### Preface

Carried out as a Student Worker at Philips GmbH, Aachen from May to Sept 2016.



##### Implementation

Worked on implementing deep learning (or CNN) based architectures for segmentation of brain tumors using Brain MRI. <br />
Primarily working on Python based scripting with machine learning frameworks like Tensorflow, Scikit-learn.

---

### Medical Imaging and XNAT Interface Development

##### Preface

Carried out as part of a mandatory internship during the master's degree at Philips GmbH, Aachen from Nov 2015 to May 2016.


##### Implementation

Worked on a project to calculate the Bone Scan Index of cancer patients, which included tasks of different bone segmentation from CT scan, 
registration of images of different modalities (CT and SPECT), calculation of BSI value.
Scripting was done based on Python using several proprietory tools and softwares owned by Philips.

A C# based user interface was implemented for the purpose of data archiving.

---

### Medical Imaging in Imalytics

##### Preface

Carried out as a Student Worker at Philips GmbH, Aachen from May to Oct 2015.


##### Implementation

Various segmentation and registration based tasks were performed on different image modalities.
Scripting was done on Python with the help of the Philips owned software Imalytics.